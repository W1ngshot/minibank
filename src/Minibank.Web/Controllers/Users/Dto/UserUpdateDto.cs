﻿namespace Minibank.Web.Controllers.Users.Dto;

public class UserUpdateDto
{
    public string Id { get; set; }
    public string Login { get; set; }
    public string Email { get; set; }
}