﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minibank.Core.Domains.Users;
using Minibank.Core.Domains.Users.Services;
using Minibank.Web.Controllers.Users.Dto;

namespace Minibank.Web.Controllers.Users;

[ApiController]
[Authorize]
[Route("[controller]")]
public class UserController : Controller
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpPost("CreateUser")]
    public async Task Create(UserDto model, CancellationToken cancellationToken)
    {
        await _userService.CreateAsync(new User
        {
            Login = model.Login.Trim(),
            Email = model.Email.Trim()
        }, cancellationToken);
    }

    [HttpPut("UpdateUser")]
    public async Task Update(UserUpdateDto model, CancellationToken cancellationToken)
    {
        await _userService.UpdateAsync(new User
        {
            Id = model.Id,
            Login = model.Login.Trim(),
            Email = model.Email.Trim()
        }, cancellationToken);
    }

    [HttpDelete("DeleteUser")]
    public async Task Delete(string id, CancellationToken cancellationToken)
    {
        await _userService.DeleteAsync(id, cancellationToken);
    }
}