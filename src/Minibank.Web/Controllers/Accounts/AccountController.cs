﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minibank.Core.Domains.Accounts.Services;
using Minibank.Web.Controllers.Accounts.Dto;

namespace Minibank.Web.Controllers.Accounts;

[ApiController]
[Authorize]
[Route("[controller]")]
public class AccountController : Controller
{
    private readonly IBankAccountService _bankAccountService;

    public AccountController(IBankAccountService bankAccountService)
    {
        _bankAccountService = bankAccountService;
    }

    [HttpPost("CreateAccount")]
    public async Task Create(AccountDto model, CancellationToken cancellationToken)
    {
        await _bankAccountService.CreateAsync(model.UserId, model.Currency, model.Money, cancellationToken);
    }

    [HttpDelete("DeactivateAccount")]
    public async Task Deactivate(string id, CancellationToken cancellationToken)
    {
        await _bankAccountService.DeactivateAsync(id, cancellationToken);
    }

    [HttpGet("CalculateCommission")]
    public async Task<double> CalculateCommission(double amount, string fromAccountId, string toAccountId, CancellationToken cancellationToken)
    {
        return await _bankAccountService.CalculateCommissionAsync(amount, fromAccountId, toAccountId, cancellationToken);
    }
    
    [HttpPost("TransferMoney")]
    public async Task TransferMoney(TransferDto model, CancellationToken cancellationToken)
    {
        await _bankAccountService.TransferMoneyAsync(model.Amount, model.FromAccountId, model.ToAccountId, cancellationToken);
    }
}