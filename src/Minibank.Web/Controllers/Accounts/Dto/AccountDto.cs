﻿namespace Minibank.Web.Controllers.Accounts.Dto;

public class AccountDto
{
    public string UserId { get; set; }
    public double Money { get; set; }
    public string Currency { get; set; }
}