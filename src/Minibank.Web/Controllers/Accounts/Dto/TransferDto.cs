﻿namespace Minibank.Web.Controllers.Accounts.Dto;

public class TransferDto
{
    public double Amount { get; set; }
    public string FromAccountId { get; set; }
    public string ToAccountId { get; set; }
}