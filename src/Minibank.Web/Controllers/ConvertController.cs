using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Minibank.Core.Exceptions;
using Minibank.Core.Logic;

namespace Minibank.Web.Controllers;

[ApiController]
[Authorize
]
[Route("[controller]")]
public class ConvertController : ControllerBase
{
    private readonly ICurrencyConverter _currencyConverter;

    public ConvertController(ICurrencyConverter currencyConverter)
    {
        _currencyConverter = currencyConverter;
    }

    [HttpGet("Convert")]
    public double Get(int amount, string from, string to)
    {
        if (!Enum.TryParse<CurrencyType>(from, out var fromCurrency) ||
            !Enum.TryParse<CurrencyType>(to, out var toCurrency))
            throw new ValidationException("������ ������� �����������");
        return _currencyConverter.Convert(amount, fromCurrency, toCurrency).GetAwaiter().GetResult();
    }
}