﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Minibank.Web.Middleware;

public class CustomAuthenticationMiddleware
{
    public readonly RequestDelegate Next;

    public CustomAuthenticationMiddleware(RequestDelegate next)
    {
        Next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var tokenString = context.Request.Headers.Authorization
            .FirstOrDefault(str => str.StartsWith("Bearer "))?
            ["Bearer ".Length..]?
            .Trim();

        if (tokenString != null)
        {
            var tokenValidToDate = GetUtcExpireDateForToken(tokenString);
            if (tokenValidToDate < DateTime.UtcNow)
            {
                context.Response.StatusCode = StatusCodes.Status403Forbidden;
                await context.Response.WriteAsJsonAsync("Срок авторизации истёк");
                return;
            }
        }

        await Next(context);
    }

    private static DateTime GetUtcExpireDateForToken(string token)
    {
        var data = token.Split('.')[1];

        var buffer = Convert.FromBase64String(data);
        var decodedData = Encoding.ASCII.GetString(buffer);

        if (!int.TryParse(decodedData.Split(',')
                .FirstOrDefault(param => param.StartsWith("\"exp\":"))?
                [6..], out var unixExpireDate))
            throw new Exception("Ошибка при парсинге jwt токена");

        return DateTimeOffset.FromUnixTimeSeconds(unixExpireDate).UtcDateTime;
    }
}