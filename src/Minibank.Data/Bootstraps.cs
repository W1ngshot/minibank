﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Minibank.Core;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.Domains.Transactions;
using Minibank.Core.Domains.Users;
using Minibank.Core.Logic;
using Minibank.Data.Repositories;
using Minibank.Data.Services;

namespace Minibank.Data;

public static class Bootstraps
{
    public static IServiceCollection AddData(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddHttpClient<ICurrencyRateService, CurrencyRateService>();
        services.AddScoped<ICurrencyRateService, CurrencyRateService>();

        services.AddScoped<IUnitOfWork, EfUnitOfWork>();
        services.AddDbContext<MinibankContext>(options => 
            options.UseNpgsql(configuration.GetRequiredSection("DataBase")["ConnectionString"])
                .UseSnakeCaseNamingConvention());

        services.AddScoped<IUserRepository, UserRepository>();
        services.AddScoped<IBankAccountRepository, BankAccountRepository>();
        services.AddScoped<ITransactionRepository, TransactionRepository>();

        return services;
    }
}