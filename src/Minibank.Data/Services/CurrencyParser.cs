﻿namespace Minibank.Data.Services;

public class CurrencyParser
{
    public DateTime Date { get; set; }
    public Dictionary<string, CurrencyItem> Valute { get; set; }
}

public class CurrencyItem
{
    public string ID { get; set; }
    public string CharCode { get; set; }
    public double Value { get; set; }
}