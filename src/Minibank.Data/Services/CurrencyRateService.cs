﻿using Minibank.Core.Exceptions;
using Minibank.Core.Logic;
using System.Net.Http.Json;

namespace Minibank.Data.Services;

public class CurrencyRateService : ICurrencyRateService
{
    private readonly HttpClient _httpClient;
    public CurrencyRateService(HttpClient httpClient)
    {
        _httpClient = httpClient;
        _httpClient.BaseAddress = new Uri("https://www.cbr-xml-daily.ru");
    }

    public async Task<double> GetRateAsync(CurrencyType currencyType)
    {
        var data = await GetRateHttpResponse() 
                   ?? throw new Exception("Получен null при парсинге валюты");
        return TryParseRate(data, currencyType);
    }

    private async Task<CurrencyParser?> GetRateHttpResponse()
    {
        try
        {
            return await _httpClient.GetFromJsonAsync<CurrencyParser>("daily_json.js");
        }
        catch (Exception ex)
        {
            throw new Exception("Невозможно получить данные с сайта", ex);
        }
    }

    private static double TryParseRate(CurrencyParser data, CurrencyType currencyType)
    {
        try
        {
            return data.Valute
                       .Select(x => x.Value)
                       .FirstOrDefault(x => x.CharCode == currencyType.ToString())?.Value 
                   ?? throw new ValidationException("Не удалось найти валюту");
        }
        catch (Exception ex)
        {
            throw new Exception("Ошибка при парсинге валюты", ex);
        }
    }
}