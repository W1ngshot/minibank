﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Minibank.Data.DbModels;

public class UserDbModel
{
    public string Id { get; set; }
    public string Login { get; set; }
    public string Email { get; set; }

    internal class Map : IEntityTypeConfiguration<UserDbModel>
    {
        public void Configure(EntityTypeBuilder<UserDbModel> builder)
        {
            builder.ToTable("user");

            builder.HasKey(x => x.Id)
                .HasName("pk_user");

            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Login)
                .IsRequired();

            builder.Property(x => x.Email)
                .IsRequired();
        }
    }
}