﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Minibank.Core;
using Minibank.Core.Logic;

namespace Minibank.Data.DbModels;

public class BankAccountDbModel
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public UserDbModel User { get; set; }
    public double Money { get; set; }
    public CurrencyType Currency { get; set; }
    public bool IsActive { get; set; }
    public DateTime OpenDate { get; set; }
    public DateTime? CloseDate { get; set; }

    internal class Map : IEntityTypeConfiguration<BankAccountDbModel>
    {
        public void Configure(EntityTypeBuilder<BankAccountDbModel> builder)
        {
            builder.ToTable("bank_account");

            builder.HasKey(x => x.Id).HasName("pk_bank_account");

            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder.Property(x => x.UserId)
                .IsRequired();

            builder.HasOne(x => x.User)
                .WithMany()
                .HasForeignKey(x => x.UserId)
                .HasConstraintName("fk_user_id");

            builder.Property(x => x.Money)
                .IsRequired();

            builder.Property(x => x.Currency)
                .IsRequired();

            builder.Property(x => x.IsActive)
                .IsRequired();

            builder.Property(x => x.OpenDate)
                .IsRequired();

            builder.Property(x => x.CloseDate);
        }
    }
}