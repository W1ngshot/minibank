﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Minibank.Core;
using Minibank.Core.Logic;

namespace Minibank.Data.DbModels;

public class TransactionDbModel
{
    public string Id { get; set; }
    public double Amount { get; set; }
    public CurrencyType Currency { get; set; }
    public string FromAccountId { get; set; }
    public BankAccountDbModel FromAccount { get; set; }
    public string ToAccountId { get; set; }
    public BankAccountDbModel ToAccount { get; set; }

    internal class Map : IEntityTypeConfiguration<TransactionDbModel>
    {
        public void Configure(EntityTypeBuilder<TransactionDbModel> builder)
        {
            builder.ToTable("transaction");

            builder.HasKey(x => x.Id)
                .HasName("pk_transaction");

            builder.Property(x => x.Id)
                .ValueGeneratedOnAdd();

            builder.Property(x => x.Amount)
                .IsRequired();

            builder.Property(x => x.Currency)
                .IsRequired();

            builder.Property(x => x.FromAccountId)
                .IsRequired();

            builder.HasOne(x => x.FromAccount)
                .WithMany()
                .HasForeignKey(x => x.FromAccountId)
                .HasConstraintName("fk_from_account_id");

            builder.Property(x => x.ToAccountId)
                .IsRequired();

            builder.HasOne(x => x.ToAccount)
                .WithMany()
                .HasForeignKey(x => x.ToAccountId)
                .HasConstraintName("fk_to_account_id");
        }
    }
}