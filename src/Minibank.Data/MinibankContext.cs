﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Minibank.Data.DbModels;

namespace Minibank.Data;

public class MinibankContext : DbContext
{
    public DbSet<BankAccountDbModel> BankAccounts { get; set; }
    public DbSet<TransactionDbModel> TransactionModels { get; set; }
    public DbSet<UserDbModel> Users { get; set; }

    public MinibankContext(DbContextOptions options) : base(options)
    {
        
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(MinibankContext).Assembly);
        base.OnModelCreating(modelBuilder);
    }
}

public class Factory : IDesignTimeDbContextFactory<MinibankContext>
{
    public MinibankContext CreateDbContext(string[] args)
    {
        var options = new DbContextOptionsBuilder()
            .UseNpgsql("FakeConnectionStringOnlyForMigrations")
            .UseSnakeCaseNamingConvention()
            .Options;
        return new MinibankContext(options);
    }
}