﻿using Microsoft.EntityFrameworkCore;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.Exceptions;
using Minibank.Core.ValidationMessages;
using Minibank.Data.DbModels;

namespace Minibank.Data.Repositories;

public class BankAccountRepository : IBankAccountRepository
{
    private readonly MinibankContext _context;

    public BankAccountRepository(MinibankContext context)
    {
        _context = context;
    }

    public async Task CreateAsync(BankAccount account, CancellationToken cancellationToken)
    {
        var entity = new BankAccountDbModel
        {
            UserId = account.UserId,
            Money = account.Money,
            Currency = account.Currency,
            IsActive = account.IsActive,
            OpenDate = account.OpenDate,
            CloseDate = account.CloseDate
        };

        await _context.BankAccounts.AddAsync(entity, cancellationToken);
    }

    public async Task UpdateAsync(BankAccount account, CancellationToken cancellationToken)
    {
        var entity = await _context.BankAccounts
                         .FirstOrDefaultAsync(x => x.Id == account.Id, cancellationToken)
                     ?? throw new ValidationException(ErrorMessages.AccountNotExists);

        entity.Money = account.Money;
        entity.CloseDate = account.CloseDate;
        entity.IsActive = account.IsActive;
    }

    public async Task<bool> HasAccountsAsync(string userId, CancellationToken cancellationToken) =>
        await _context.BankAccounts
            .AnyAsync(x => x.UserId == userId, cancellationToken);

    public async Task<BankAccount?> GetAccountByIdAsync(string id, CancellationToken cancellationToken)
    {
        var bankAccount = await _context.BankAccounts
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        return bankAccount is null
            ? null
            : new BankAccount
            {
                Id = bankAccount.Id,
                UserId = bankAccount.UserId,
                Money = bankAccount.Money,
                Currency = bankAccount.Currency,
                IsActive = bankAccount.IsActive,
                OpenDate = bankAccount.OpenDate,
                CloseDate = bankAccount.CloseDate
            };
    }
}