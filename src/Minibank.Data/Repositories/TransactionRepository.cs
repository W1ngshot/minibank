﻿using Minibank.Core.Domains.Transactions;
using Minibank.Data.DbModels;

namespace Minibank.Data.Repositories;

public class TransactionRepository : ITransactionRepository
{
    private readonly MinibankContext _context;

    public TransactionRepository(MinibankContext context)
    {
        _context = context;
    }

    public async Task AddTransactionAsync(Transaction transaction, CancellationToken cancellationToken)
    {
        await _context.TransactionModels.AddAsync(new TransactionDbModel
        {
            Amount = transaction.Amount,
            Currency = transaction.Currency,
            FromAccountId = transaction.FromAccountId,
            ToAccountId = transaction.ToAccountId
        }, cancellationToken);
    }
}