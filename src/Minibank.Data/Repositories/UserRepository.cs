﻿using Microsoft.EntityFrameworkCore;
using Minibank.Core.Domains.Users;
using Minibank.Core.Exceptions;
using Minibank.Core.ValidationMessages;
using Minibank.Data.DbModels;

namespace Minibank.Data.Repositories;

public class UserRepository : IUserRepository
{
    private readonly MinibankContext _context;

    public UserRepository(MinibankContext context)
    {
        _context = context;
    }

    public async Task CreateAsync(User user, CancellationToken cancellationToken)
    {
        var entity = new UserDbModel
        {
            Login = user.Login,
            Email = user.Email
        };

        await _context.Users.AddAsync(entity, cancellationToken);
    }

    public async Task UpdateAsync(User user, CancellationToken cancellationToken)
    {
        var entity = await _context.Users
                         .FirstOrDefaultAsync(x => x.Id == user.Id, cancellationToken) 
                     ?? throw new ValidationException(ErrorMessages.UserNotExists);

        entity.Login = user.Login;
        entity.Email = user.Email;
    }

    public async Task DeleteAsync(string id, CancellationToken cancellationToken)
    {
        var entity = await _context.Users
                         .FirstOrDefaultAsync(x => x.Id == id, cancellationToken) 
                     ?? throw new ValidationException(ErrorMessages.UserNotExists);

        _context.Users.Remove(entity);
    }

    public async Task<bool> IsExistsAsync(string id, CancellationToken cancellationToken) => 
        await _context.Users.AnyAsync(x => x.Id == id, cancellationToken);

    public async Task<bool> IsLoginOrEmailRegisteredAsync(string login, string email, CancellationToken cancellationToken, string? excludingUserId = null) =>
        await _context.Users.AsNoTracking()
            .Where(x => x.Id != excludingUserId)
            .AnyAsync(x => x.Login == login || x.Email == email, cancellationToken);

    public async Task<User?> GetUserByIdAsync(string id, CancellationToken cancellationToken)
    {
        var user = await _context.Users
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

        return user is null
            ? null
            : new User
            {
                Id = user.Id,
                Login = user.Login,
                Email= user.Email
            };
    }
}