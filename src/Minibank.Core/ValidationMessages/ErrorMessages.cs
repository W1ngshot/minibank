﻿namespace Minibank.Core.ValidationMessages;

public class ErrorMessages
{
    public const string EmptyLogin = "Поле логина не может быть пустым";
    public const string TooShortLogin = "Логин слишком короткий";
    public const string TooLongLogin = "Логин слишком длинный";
    public const string LoginContainsWrongSymbols = "Логин содержит недопустимые символы";
    public const string EmptyEmail = "Поле электронной почты не может быть пустым";
    public const string IncorrectEmail = "Почта введена неверно";
    public const string LoginOrEmailAlreadyExists = "Данный логин или почта уже были зарегистрированы";
    public const string WrongAmountOfMoney = "Неверная сумма";
    public const string WrongCurrency = "Валюта введена неверно";

    public const string UserHasAccounts = "У данного пользователя есть аккаунты";
    public const string UserNotExists = "Данного пользователя не существует";

    public const string AccountIsNotActive = "Данный аккаунт не активен";
    public const string AccountDeactivatingWithMoney = "На счёте остались деньги";
    public const string AccountNotExists = "Данного аккаунта не существует";
    public const string AccountNotEnoughMoney = "Недостаточно средств";
    public const string AccountsMatch = "Указан один аккаунт";

    public const string ValidatorTestError = "Валидатор должен быть вызван";
}