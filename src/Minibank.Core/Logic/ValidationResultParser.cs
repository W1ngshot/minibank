﻿using FluentValidation.Results;
using Minibank.Core.Exceptions;
using System.Text;

namespace Minibank.Core.Logic;

public class ValidationResultParser : IValidationResultParser
{
    public void ParseValidationResultAndThrow(ValidationResult? validationResult)
    {
        if (validationResult == null)
        {
            return;
        }

        if (validationResult.Errors.Any())
        {
            throw new ValidationException(validationResult.Errors
                .Select(x => x.ErrorMessage)
                .Aggregate(new StringBuilder(), (currentString, next) => currentString.AppendLine(next),
                    currentString => currentString.ToString()));
        }
    }
}