﻿using Minibank.Core.Exceptions;

namespace Minibank.Core.Logic;

public class CurrencyConverter : ICurrencyConverter
{
    private readonly ICurrencyRateService _currencyRateService;

    public CurrencyConverter(ICurrencyRateService currencyRateService)
    {
        _currencyRateService = currencyRateService;
    }

    public async Task<double> Convert(double amount, CurrencyType fromCurrency, CurrencyType toCurrency)
    {
        double result;
        if (fromCurrency == CurrencyType.RUB)
        {
            result = amount / await _currencyRateService.GetRateAsync(toCurrency);
        }
        else if (toCurrency == CurrencyType.RUB)
        {
            result = amount * await _currencyRateService.GetRateAsync(fromCurrency);
        }
        else
        {
            result = await Convert(amount * await _currencyRateService.GetRateAsync(fromCurrency), CurrencyType.RUB,
                toCurrency);
        }

        return result >= 0 ? 
            Math.Round(result, 2, MidpointRounding.ToNegativeInfinity) 
            : throw new ValidationException("Отрицательная сумма");
    }
}