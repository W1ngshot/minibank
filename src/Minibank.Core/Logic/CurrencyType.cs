﻿namespace Minibank.Core.Logic;

public enum CurrencyType
{
    RUB,
    USD,
    EUR
}