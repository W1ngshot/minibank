﻿using Minibank.Core.Domains.Accounts;

namespace Minibank.Core.Logic;

public class CommissionCounter : ICommissionCounter
{
    private readonly ICurrencyConverter _currencyConverter;

    public CommissionCounter(ICurrencyConverter currencyConverter)
    {
        _currencyConverter = currencyConverter;
    }

    public double CalculateCommission(double amount, BankAccount fromAccount, BankAccount toAccount)
    {
        return fromAccount.UserId == toAccount.UserId ?
            0 : Math.Round(amount * 0.02, 2, MidpointRounding.ToNegativeInfinity);
    }

    public async Task<double> CalculateAmountWithCommission(double amount, BankAccount fromAccount, BankAccount toAccount)
    {
        var commission = CalculateCommission(amount, fromAccount, toAccount);
        var amountWithCommission = fromAccount.Currency != toAccount.Currency
            ? await _currencyConverter.Convert(amount - commission, fromAccount.Currency, toAccount.Currency)
            : amount - commission;
        return amountWithCommission;
    }
}