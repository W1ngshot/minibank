﻿namespace Minibank.Core.Logic;

public interface ICurrencyConverter
{
    public Task<double> Convert(double amount, CurrencyType fromCurrency, CurrencyType toCurrency);
}