﻿using Minibank.Core.Domains.Accounts;

namespace Minibank.Core.Logic;
public interface ICommissionCounter
{
    public double CalculateCommission(double amount, BankAccount fromAccount, BankAccount toAccount);

    public Task<double> CalculateAmountWithCommission(double amount, BankAccount fromAccount, BankAccount toAccount);
}