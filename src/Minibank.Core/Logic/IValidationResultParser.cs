﻿using FluentValidation.Results;

namespace Minibank.Core.Logic;

public interface IValidationResultParser
{
    void ParseValidationResultAndThrow(ValidationResult result);
}