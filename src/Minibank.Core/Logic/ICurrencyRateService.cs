﻿namespace Minibank.Core.Logic;

public interface ICurrencyRateService
{
    public Task<double> GetRateAsync(CurrencyType currencyType);
}