﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Minibank.Core.Domains.Accounts.Services;
using Minibank.Core.Domains.Accounts.Validators;
using Minibank.Core.Domains.Users.Services;
using Minibank.Core.Logic;

namespace Minibank.Core;

public static class Bootstraps
{
    public static IServiceCollection AddCore(this IServiceCollection services)
    {
        services.AddScoped<ICurrencyConverter, CurrencyConverter>();
        services.AddScoped<IUserService, UserService>();
        services.AddScoped<IBankAccountService, BankAccountService>();
        services.AddFluentValidation().AddValidatorsFromAssembly(typeof(UserService).Assembly);
        services.AddScoped<IValidationResultParser, ValidationResultParser>();
        services.AddScoped<IDateTimeProvider, DateTimeProvider>();
        services.AddScoped<ICommissionCounter, CommissionCounter>();
        services.AddScoped<IAccountValidatingMethods, AccountsValidatingMethods>();
        return services;
    }
}