﻿using FluentValidation;
using Minibank.Core.ValidationMessages;

namespace Minibank.Core.Domains.Users.Validators;

public class UserDataValidator : AbstractValidator<User>
{
    public UserDataValidator()
    {
        RuleFor(user => user.Login)
            .NotEmpty()
            .WithMessage(ErrorMessages.EmptyLogin);
        RuleFor(user => user.Login)
            .MinimumLength(3)
            .WithMessage(ErrorMessages.TooShortLogin);
        RuleFor(user => user.Login)
            .MaximumLength(20)
            .WithMessage(ErrorMessages.TooLongLogin);
        RuleFor(user => user.Login)
            .Matches(@"^(\d|[a-zA-z])+$")
            .WithMessage(ErrorMessages.LoginContainsWrongSymbols);

        RuleFor(user => user.Email)
            .NotEmpty()
            .WithMessage(ErrorMessages.EmptyEmail);
        RuleFor(user => user.Email)
            .Matches(@"^[^@\s]+@[^@\s]+\.[^@\s]+$")
            .WithMessage(ErrorMessages.IncorrectEmail);
    }
}