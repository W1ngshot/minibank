﻿using FluentValidation;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.ValidationMessages;
using ValidationException = Minibank.Core.Exceptions.ValidationException;

namespace Minibank.Core.Domains.Users.Services;

public class UserService : IUserService
{
    #region initialize info
    private readonly IUserRepository _userRepository;
    private readonly IBankAccountRepository _bankAccountRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValidator<User> _userDataValidator;

    public UserService(IUserRepository userRepository,
        IBankAccountRepository bankAccountRepository,
        IUnitOfWork unitOfWork, 
        IValidator<User> userDataValidator)
    {
        _userRepository = userRepository;
        _bankAccountRepository = bankAccountRepository;
        _unitOfWork = unitOfWork;
        _userDataValidator = userDataValidator;
    }
    #endregion

    public async Task CreateAsync(User user, CancellationToken cancellationToken)
    {
        await _userDataValidator.ValidateAndThrowAsync(user, cancellationToken);

        await EnsureLoginOrEmailAreNotRegisteredAsync(user, cancellationToken);

        await _userRepository.CreateAsync(user, cancellationToken);
        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }

    public async Task UpdateAsync(User user, CancellationToken cancellationToken)
    {
        await UserExistenceValidateAndThrowAsync(user.Id, cancellationToken);
        await _userDataValidator.ValidateAndThrowAsync(user, cancellationToken);

        await EnsureLoginOrEmailAreNotRegisteredAsync(user, cancellationToken);

        await _userRepository.UpdateAsync(user, cancellationToken);
        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }

    public async Task DeleteAsync(string userId, CancellationToken cancellationToken)
    {
        await UserExistenceValidateAndThrowAsync(userId, cancellationToken);
        await EnsureUserDoesNotHaveAccountsAsync(userId, cancellationToken);

        await _userRepository.DeleteAsync(userId, cancellationToken);
        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }

    private async Task UserExistenceValidateAndThrowAsync(string userId, CancellationToken cancellationToken)
    {
        if (!await _userRepository.IsExistsAsync(userId, cancellationToken))
        {
            throw new ValidationException(ErrorMessages.UserNotExists);
        }
    }

    private async Task EnsureUserDoesNotHaveAccountsAsync(string userId, CancellationToken cancellationToken)
    {
        if (await _bankAccountRepository.HasAccountsAsync(userId, cancellationToken))
        {
            throw new ValidationException(ErrorMessages.UserHasAccounts);
        }
    }

    private async Task EnsureLoginOrEmailAreNotRegisteredAsync(User user, CancellationToken cancellationToken)
    {
        if (await _userRepository.IsLoginOrEmailRegisteredAsync(user.Login, user.Email, cancellationToken, user.Id))
        {
            throw new ValidationException(ErrorMessages.LoginOrEmailAlreadyExists);
        }
    }
}