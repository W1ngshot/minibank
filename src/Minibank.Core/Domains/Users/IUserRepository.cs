﻿namespace Minibank.Core.Domains.Users;

public interface IUserRepository
{
    Task CreateAsync(User user, CancellationToken cancellationToken);
    Task UpdateAsync(User user, CancellationToken cancellationToken);
    Task DeleteAsync(string id, CancellationToken cancellationToken);
    Task<bool> IsExistsAsync(string id, CancellationToken cancellationToken);
    public Task<bool> IsLoginOrEmailRegisteredAsync(string login, string email, CancellationToken cancellationToken, string? excludingUserId = null);
    Task<User?> GetUserByIdAsync(string id, CancellationToken cancellationToken);
}