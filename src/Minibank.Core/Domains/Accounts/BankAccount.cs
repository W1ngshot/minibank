﻿using Minibank.Core.Logic;

namespace Minibank.Core.Domains.Accounts;

public class BankAccount
{
    public string Id { get; set; }
    public string UserId { get; set; }
    public double Money { get; set; }
    public CurrencyType Currency { get; set; }
    public bool IsActive { get; set; }
    public DateTime OpenDate { get; set; }
    public DateTime? CloseDate { get; set; }
}