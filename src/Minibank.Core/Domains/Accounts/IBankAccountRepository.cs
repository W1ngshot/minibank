﻿namespace Minibank.Core.Domains.Accounts;

public interface IBankAccountRepository
{
    Task CreateAsync(BankAccount account, CancellationToken cancellationToken);
    Task UpdateAsync(BankAccount account, CancellationToken cancellationToken);
    Task<bool> HasAccountsAsync(string userId, CancellationToken cancellationToken);
    Task<BankAccount?> GetAccountByIdAsync(string id, CancellationToken cancellationToken);
}