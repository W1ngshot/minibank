﻿using FluentValidation;
using Minibank.Core.Domains.Accounts.Validators;
using Minibank.Core.Domains.Transactions;
using Minibank.Core.Logic;
using Minibank.Core.ValidationMessages;
using ValidationException = Minibank.Core.Exceptions.ValidationException;

namespace Minibank.Core.Domains.Accounts.Services;

public class BankAccountService : IBankAccountService
{
    #region initialize info
    private readonly IBankAccountRepository _bankAccountRepository;
    private readonly IAccountValidatingMethods _accountValidatingMethods;
    private readonly ITransactionRepository _transactionRepository;
    private readonly ICommissionCounter _commissionCounter;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValidator<BankAccount> _deactivateAccountValidator;
    private readonly IDateTimeProvider _dateTimeProvider;

    public BankAccountService(IBankAccountRepository bankAccountRepository, 
        IAccountValidatingMethods accountValidatingMethods,
        ITransactionRepository transactionRepository,
        ICommissionCounter commissionCounter,
        IUnitOfWork unitOfWork,
        IValidator<BankAccount> deactivateAccountValidator,
        IDateTimeProvider dateTimeProvider)
    {
        _bankAccountRepository = bankAccountRepository;
        _accountValidatingMethods = accountValidatingMethods;
        _transactionRepository = transactionRepository;
        _commissionCounter = commissionCounter;
        _unitOfWork = unitOfWork;
        _deactivateAccountValidator = deactivateAccountValidator;
        _dateTimeProvider = dateTimeProvider;
    }
    #endregion

    public async Task CreateAsync(string userId, string stringCurrency, double money, CancellationToken cancellationToken)
    {
        var currency = _accountValidatingMethods.AmountAndCurrencyValidateAndThrow(stringCurrency, money);
        await _accountValidatingMethods.EnsureUserExistsAsync(userId, cancellationToken);

        await _bankAccountRepository.CreateAsync(new BankAccount
        {
            UserId = userId,
            Currency = currency,
            Money = money,
            IsActive = true,
            OpenDate = _dateTimeProvider.UtcNow,
            CloseDate = null
        }, cancellationToken);

        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }

    public async Task DeactivateAsync(string id, CancellationToken cancellationToken)
    {
        var bankAccount = await _accountValidatingMethods.GetActiveAccountAsync(id, cancellationToken);

        await _deactivateAccountValidator.ValidateAndThrowAsync(bankAccount, cancellationToken);

        bankAccount.IsActive = false;
        bankAccount.CloseDate = _dateTimeProvider.UtcNow;

        await _bankAccountRepository.UpdateAsync(bankAccount, cancellationToken);
        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }


    public async Task<double> CalculateCommissionAsync(double amount, string fromAccountId, string toAccountId, CancellationToken cancellationToken)
    {
        _accountValidatingMethods.CheckForSameAccountsAndThrow(fromAccountId, toAccountId);
        _accountValidatingMethods.EnsurePositive(amount);

        var firstAccount = await _accountValidatingMethods.GetActiveAccountAsync(fromAccountId, cancellationToken);
        var secondAccount = await _accountValidatingMethods.GetActiveAccountAsync(toAccountId, cancellationToken);

        return _commissionCounter.CalculateCommission(amount, firstAccount, secondAccount);
    }

    public async Task TransferMoneyAsync(double amount, string fromAccountId, string toAccountId, CancellationToken cancellationToken)
    {
        _accountValidatingMethods.CheckForSameAccountsAndThrow(fromAccountId, toAccountId);
        _accountValidatingMethods.EnsurePositive(amount);

        var firstAccount = await _accountValidatingMethods.GetActiveAccountAsync(fromAccountId, cancellationToken);
        var secondAccount = await _accountValidatingMethods.GetActiveAccountAsync(toAccountId, cancellationToken);

        _accountValidatingMethods.EnsureAccountHasEnoughMoney(amount, firstAccount);

        var amountWithCommission = await _commissionCounter.CalculateAmountWithCommission(amount, firstAccount, secondAccount);

       await _transactionRepository.AddTransactionAsync(new Transaction
        {
            Amount = amount,
            Currency = firstAccount.Currency,
            FromAccountId = fromAccountId,
            ToAccountId = toAccountId
        }, cancellationToken);

        firstAccount.Money -= amount;
        secondAccount.Money += amountWithCommission;

        await _bankAccountRepository.UpdateAsync(firstAccount, cancellationToken);
        await _bankAccountRepository.UpdateAsync(secondAccount, cancellationToken);
        await _unitOfWork.SaveChangesAsync(cancellationToken);
    }
}