﻿namespace Minibank.Core.Domains.Accounts.Services;

public interface IBankAccountService
{
    Task CreateAsync(string userId, string currency, double money, CancellationToken cancellationToken);
    Task DeactivateAsync(string id, CancellationToken cancellationToken);
    Task<double> CalculateCommissionAsync(double amount, string fromAccountId, string toAccountId, CancellationToken cancellationToken);
    Task TransferMoneyAsync(double amount, string fromAccountId, string toAccountId, CancellationToken cancellationToken);
}