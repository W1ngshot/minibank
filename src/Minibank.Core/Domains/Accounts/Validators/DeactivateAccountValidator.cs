﻿using FluentValidation;
using Minibank.Core.ValidationMessages;

namespace Minibank.Core.Domains.Accounts.Validators;

public class DeactivateAccountValidator : AbstractValidator<BankAccount>
{
    public DeactivateAccountValidator()
    {
        RuleFor(account => account.IsActive)
            .Must(active => active)
            .WithMessage(ErrorMessages.AccountIsNotActive);

        RuleFor(account => account.Money)
            .Empty()
            .WithMessage(ErrorMessages.AccountDeactivatingWithMoney);
    }
}