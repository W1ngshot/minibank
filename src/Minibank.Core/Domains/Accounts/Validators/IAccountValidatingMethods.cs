﻿using Minibank.Core.Logic;

namespace Minibank.Core.Domains.Accounts.Validators;

public interface IAccountValidatingMethods
{
    public Task<BankAccount> GetActiveAccountAsync(string accountId, CancellationToken cancellationToken);

    public void CheckForSameAccountsAndThrow(string fromAccountId, string toAccountId);

    public CurrencyType AmountAndCurrencyValidateAndThrow(string stringCurrency, double money);

    public Task EnsureUserExistsAsync(string userId, CancellationToken cancellationToken);

    public void EnsurePositive(double amount);

    public void EnsureAccountHasEnoughMoney(double amount, BankAccount account);
}