﻿using Minibank.Core.Domains.Users;
using Minibank.Core.Exceptions;
using Minibank.Core.Logic;
using Minibank.Core.ValidationMessages;

namespace Minibank.Core.Domains.Accounts.Validators;

public class AccountsValidatingMethods : IAccountValidatingMethods
{
    private readonly IBankAccountRepository _bankAccountRepository;
    private readonly IUserRepository _userRepository;

    public AccountsValidatingMethods(IBankAccountRepository bankAccountRepository, IUserRepository userRepository)
    {
        _bankAccountRepository = bankAccountRepository;
        _userRepository = userRepository;
    }

    public async Task<BankAccount> GetActiveAccountAsync(string accountId, CancellationToken cancellationToken)
    {
        var account = await _bankAccountRepository.GetAccountByIdAsync(accountId, cancellationToken)
                      ?? throw new ValidationException(ErrorMessages.AccountNotExists);

        return account.IsActive ? account : throw new ValidationException(ErrorMessages.AccountIsNotActive);
    }

    public void CheckForSameAccountsAndThrow(string fromAccountId, string toAccountId)
    {
        if (fromAccountId == toAccountId)
        {
            throw new ValidationException(ErrorMessages.AccountsMatch);
        }
    }

    public CurrencyType AmountAndCurrencyValidateAndThrow(string stringCurrency, double money)
    {
        if (!Enum.TryParse<CurrencyType>(stringCurrency, out var currency))
        {
            throw new ValidationException(ErrorMessages.WrongCurrency);
        }

        if (money < 0)
        {
            throw new ValidationException(ErrorMessages.WrongAmountOfMoney);
        }

        return currency;
    }

    public async Task EnsureUserExistsAsync(string userId, CancellationToken cancellationToken)
    {
        if (!await _userRepository.IsExistsAsync(userId, cancellationToken))
        {
            throw new ValidationException(ErrorMessages.UserNotExists);
        }
    }

    public void EnsurePositive(double amount)
    {
        if (amount <= 0)
        {
            throw new ValidationException(ErrorMessages.WrongAmountOfMoney);
        }
    }

    public void EnsureAccountHasEnoughMoney(double amount, BankAccount account)
    {
        if (amount > account.Money)
        {
            throw new ValidationException(ErrorMessages.AccountNotEnoughMoney);
        }
    }
}