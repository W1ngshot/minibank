﻿namespace Minibank.Core.Domains.Transactions;

public interface ITransactionRepository
{
    public Task AddTransactionAsync(Transaction transaction, CancellationToken cancellationToken);
}