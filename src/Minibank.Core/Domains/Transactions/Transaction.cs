﻿using Minibank.Core.Logic;

namespace Minibank.Core.Domains.Transactions;

public class Transaction
{
    public string Id { get; set; }
    public double Amount { get; set; }
    public CurrencyType Currency { get; set; }
    public string FromAccountId { get; set; }
    public string ToAccountId { get; set; }
}