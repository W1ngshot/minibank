﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.Domains.Accounts.Validators;
using Minibank.Core.Domains.Users;
using Minibank.Core.Exceptions;
using Minibank.Core.Logic;
using Minibank.Core.ValidationMessages;
using Moq;
using Xunit;

namespace Minibank.Core.Tests.Domains.Accounts.Validators;

public class AccountValidatingMethodsTests
{
    private readonly Mock<IBankAccountRepository> _bankAccountRepositoryMock;
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly IAccountValidatingMethods _accountValidatingMethods;

    public AccountValidatingMethodsTests()
    {
        _bankAccountRepositoryMock = new Mock<IBankAccountRepository>();
        _userRepositoryMock = new Mock<IUserRepository>();
        _accountValidatingMethods =
            new AccountsValidatingMethods(_bankAccountRepositoryMock.Object, _userRepositoryMock.Object);
    }

    [Fact]
    public async Task GetActiveAccountAsync_AccountNotExists_ShouldThrowException()
    {
        const string accountId = "TestAccountId";
        BankAccount? returningAccount = null;
        _bankAccountRepositoryMock.Setup(accountRepository =>
            accountRepository.GetAccountByIdAsync(accountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(returningAccount);

        var exception = await Assert.ThrowsAsync<ValidationException>(() =>
            _accountValidatingMethods.GetActiveAccountAsync(accountId, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.AccountNotExists, exception.Message);
    }

    [Fact]
    public async Task GetActiveAccountAsync_AccountNotActive_ShouldThrowException()
    {
        const string accountId = "TestAccountId";
        var returningAccount = new BankAccount {IsActive = false};
        _bankAccountRepositoryMock.Setup(accountRepository =>
                accountRepository.GetAccountByIdAsync(accountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(returningAccount);

        var exception = await Assert.ThrowsAsync<ValidationException>(() =>
            _accountValidatingMethods.GetActiveAccountAsync(accountId, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.AccountIsNotActive, exception.Message);
    }

    [Fact]
    public async Task GetActiveAccountAsync_SuccessPath_ShouldReturnRightBankAccount()
    {
        const string accountId = "TestAccountId";
        var expectedAccount = new BankAccount {IsActive = true};
        _bankAccountRepositoryMock.Setup(accountRepository =>
                accountRepository.GetAccountByIdAsync(accountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(expectedAccount);

        var result = await _accountValidatingMethods.GetActiveAccountAsync(accountId, It.IsAny<CancellationToken>());

        Assert.Equal(expectedAccount, result);
    }

    [Fact]
    public void CheckForSameAccountsAndThrow_SameAccountIds_ShouldThrowException()
    {
        const string sameAccountId = "TestAccountId";

        var exception = Assert.Throws<ValidationException>(() =>
            _accountValidatingMethods.CheckForSameAccountsAndThrow(sameAccountId, sameAccountId));

        Assert.Equal(ErrorMessages.AccountsMatch, exception.Message);
    }

    [Fact]
    public void AmountAndCurrencyValidateAndThrow_WrongCurrency_ShouldThrowException()
    {
        const string wrongCurrency = "WrongTestCurrencyString";

        var exception = Assert.Throws<ValidationException>(() =>
            _accountValidatingMethods.AmountAndCurrencyValidateAndThrow(wrongCurrency, It.IsAny<double>()));

        Assert.Equal(ErrorMessages.WrongCurrency, exception.Message);
    }

    [Fact]
    public void AmountAndCurrencyValidateAndThrow_WrongAmountOdMoney_ShouldThrowException()
    {
        const double wrongMoney = -1;

        var exception = Assert.Throws<ValidationException>(() =>
            _accountValidatingMethods.AmountAndCurrencyValidateAndThrow(It.IsAny<CurrencyType>().ToString(), wrongMoney));

        Assert.Equal(ErrorMessages.WrongAmountOfMoney, exception.Message);
    }

    [Fact]
    public void AmountAndCurrencyValidateAndThrow_SuccessPath_ShouldReturnCurrency()
    {
        var rightCurrency = Enum.GetValues<CurrencyType>().First();

        var result =
            _accountValidatingMethods.AmountAndCurrencyValidateAndThrow(rightCurrency.ToString(), It.IsAny<double>());

        Assert.Equal(rightCurrency, result);
    }

    [Fact]
    public async Task EnsureUserExistsAsync_UserNotExists_ShouldThrowException()
    {
        const string userId = "TestUserId";
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(userId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(false);

        var exception = await Assert.ThrowsAsync<ValidationException>(() =>
            _accountValidatingMethods.EnsureUserExistsAsync(userId, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.UserNotExists, exception.Message);
    }

    //public void EnsurePositive(double amount)
    //{
    //    if (amount <= 0)
    //    {
    //        throw new ValidationException(ErrorMessages.WrongAmountOfMoney);
    //    }
    //}

    [Theory]
    [InlineData(-1)]
    [InlineData(0)]
    public void EnsurePositive_WrongAmount_ShouldThrowException(double amount)
    {
        var exception = Assert.Throws<ValidationException>(() =>
            _accountValidatingMethods.EnsurePositive(amount));

        Assert.Equal(ErrorMessages.WrongAmountOfMoney, exception.Message);
    }

    [Fact]
    public void EnsureAccountHasEnoughMoney_AccountHasNotEnoughMoney_ShouldThrowException()
    {
        const int requiredAmount = 10;
        var account = new BankAccount {Money = requiredAmount - 1};

        var exception = Assert.Throws<ValidationException>(() =>
            _accountValidatingMethods.EnsureAccountHasEnoughMoney(requiredAmount, account));

        Assert.Equal(ErrorMessages.AccountNotEnoughMoney, exception.Message);
    }
}