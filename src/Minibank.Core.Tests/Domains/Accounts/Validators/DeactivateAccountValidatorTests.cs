﻿using System.Linq;
using System.Threading.Tasks;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.Domains.Accounts.Validators;
using Minibank.Core.ValidationMessages;
using Xunit;

namespace Minibank.Core.Tests.Domains.Accounts.Validators;

public class DeactivateAccountValidatorTests
{
    private readonly DeactivateAccountValidator _validator;

    public DeactivateAccountValidatorTests()
    {
        _validator = new DeactivateAccountValidator();
    }

    [Fact]
    public async Task DeactivateAccountValidator_AccountNotActive_ShouldHaveExpectedError()
    {
        var account = new BankAccount {IsActive = false};
        const string expectedResult = ErrorMessages.AccountIsNotActive;

            var result = await _validator.ValidateAsync(account);

        Assert.True(result.Errors?.Select(x => x.ErrorMessage).Contains(expectedResult));
    }

    [Theory]
    [InlineData(-1)]
    [InlineData(1)]
    [InlineData(1000)]
    public async Task DeactivateAccountValidator_AccountHasMoney_ShouldHaveExpectedError(double wrongMoney)
    {
        var account = new BankAccount {IsActive = true, Money = wrongMoney};
        const string expectedResult = ErrorMessages.AccountDeactivatingWithMoney;

        var result = await _validator.ValidateAsync(account);

        Assert.True(result.Errors?.Select(x => x.ErrorMessage).Contains(expectedResult));
    }
}