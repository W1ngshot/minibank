﻿using FluentValidation;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.Domains.Accounts.Services;
using Minibank.Core.Domains.Accounts.Validators;
using Minibank.Core.Domains.Transactions;
using Minibank.Core.Logic;
using Moq;
using System;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Minibank.Core.Tests.Domains.Accounts.Services;

public class BankAccountServiceTests
{
    private readonly Mock<IBankAccountRepository> _bankAccountRepositoryMock;
    private readonly Mock<IAccountValidatingMethods> _accountValidatingMethodsMock;
    private readonly Mock<ITransactionRepository> _transactionRepositoryMock;
    private readonly Mock<ICommissionCounter> _commissionCounterMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IValidator<BankAccount>> _deactivateAccountValidatorMock;
    private readonly Mock<IDateTimeProvider> _dateTimeProviderMock;
    private readonly IBankAccountService _bankAccountService;

    public BankAccountServiceTests()
    {

    _bankAccountRepositoryMock = new Mock<IBankAccountRepository>();
    _accountValidatingMethodsMock = new Mock<IAccountValidatingMethods>();
    _transactionRepositoryMock = new Mock<ITransactionRepository>();
    _commissionCounterMock = new Mock<ICommissionCounter>();
    _unitOfWorkMock = new Mock<IUnitOfWork>();
    _deactivateAccountValidatorMock = new Mock<IValidator<BankAccount>>();
    _dateTimeProviderMock = new Mock<IDateTimeProvider>();

    _bankAccountService = new BankAccountService(_bankAccountRepositoryMock.Object,
        _accountValidatingMethodsMock.Object,
        _transactionRepositoryMock.Object,
        _commissionCounterMock.Object,
        _unitOfWorkMock.Object,
        _deactivateAccountValidatorMock.Object,
        _dateTimeProviderMock.Object);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallAmountAndCurrencyValidationOnce()
    {
        const string currency = "SomeCurrencyString";
        const int amount = 10;
        _accountValidatingMethodsMock.Setup(validatingMethods =>
            validatingMethods.AmountAndCurrencyValidateAndThrow(currency, amount))
            .Returns(It.IsAny<CurrencyType>());

        await _bankAccountService.CreateAsync(It.IsAny<string>(), currency, amount, It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(
            validatingMethods => validatingMethods.AmountAndCurrencyValidateAndThrow(currency, amount), Times.Once);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallEnsureUserExistsAsyncOnce()
    {
        const string userId = "TestUserId";
        _accountValidatingMethodsMock.Setup(validatingMethods =>
            validatingMethods.EnsureUserExistsAsync(userId, It.IsAny<CancellationToken>()));

        await _bankAccountService.CreateAsync(userId, It.IsAny<CurrencyType>().ToString(), It.IsAny<double>(),
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(
            validatingMethods => validatingMethods.EnsureUserExistsAsync(userId, It.IsAny<CancellationToken>()),
            Times.Once);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallCreateOnce()
    {
        await _bankAccountService.CreateAsync(It.IsAny<string>(), It.IsAny<CurrencyType>().ToString(),
            It.IsAny<double>(),
            It.IsAny<CancellationToken>());

        _bankAccountRepositoryMock.Verify(accountRepository => 
                accountRepository.CreateAsync(It.IsAny<BankAccount>(), It.IsAny<CancellationToken>()),
            Times.Once);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallSaveChangesOnce()
    {
        await _bankAccountService.CreateAsync(It.IsAny<string>(), It.IsAny<CurrencyType>().ToString(),
            It.IsAny<double>(),
            It.IsAny<CancellationToken>());

        _unitOfWorkMock.Verify(unitOfWork => unitOfWork.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task DeactivateAsync_SuccessPath_ShouldCallGetActiveAccountAsyncWithRightId()
    {
        const string accountId = "TestAccountId";
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(accountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());

        await _bankAccountService.DeactivateAsync(accountId, It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(validatingMethods =>
            validatingMethods.GetActiveAccountAsync(accountId, It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task DeactivateAsync_SuccessPath_ShouldCallValidatorOnce()
    {
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());

        await _bankAccountService.DeactivateAsync(It.IsAny<string>(), It.IsAny<CancellationToken>());

        _deactivateAccountValidatorMock.Verify(
            validator =>
                validator.ValidateAsync(It.IsAny<ValidationContext<BankAccount>>(), It.IsAny<CancellationToken>()), 
            Times.Once);
    }

    [Fact]
    public async Task DeactivateAsync_SuccessPath_ShouldCallUpdateWithRightAccountOnce()
    {
        var expectedAccount = new BankAccount();
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(expectedAccount);

        await _bankAccountService.DeactivateAsync(It.IsAny<string>(), It.IsAny<CancellationToken>());

        _bankAccountRepositoryMock.Verify(accountRepository =>
            accountRepository.UpdateAsync(expectedAccount, It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task DeactivateAsync_SuccessPath_UpdateCallShouldChangeActiveStateAndCloseDate()
    {
        var account = new BankAccount
        {
            IsActive = true,
            CloseDate = null
        };
        var expectedCloseDate = DateTime.UtcNow;
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(account);
        _dateTimeProviderMock.Setup(dateTimeProvider => dateTimeProvider.UtcNow)
            .Returns(expectedCloseDate);

        await _bankAccountService.DeactivateAsync(It.IsAny<string>(), It.IsAny<CancellationToken>());

        Assert.False(account.IsActive);
        Assert.Equal(expectedCloseDate, account.CloseDate);
    }

    [Fact]
    public async Task DeactivateAsync_SuccessPath_UpdateCallShouldNotAffectOtherData()
    {
        const string expectedId = "TestAccountId";
        const CurrencyType expectedCurrency = CurrencyType.USD;
        const string expectedUserId = "TestUserId";
        const double expectedMoney = 0;
        var expectedOpenDate = DateTime.UtcNow;

        var account = new BankAccount
        {
            Id = expectedId,
            Currency = expectedCurrency,
            UserId = expectedUserId,
            IsActive = true,
            Money = expectedMoney,
            OpenDate = expectedOpenDate,
            CloseDate = null
        };
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(account);

        await _bankAccountService.DeactivateAsync(It.IsAny<string>(), It.IsAny<CancellationToken>());

        Assert.Equal(expectedId, account.Id);
        Assert.Equal(expectedUserId, account.UserId);
        Assert.Equal(expectedMoney, account.Money);
        Assert.Equal(expectedCurrency, account.Currency);
        Assert.Equal(expectedOpenDate, account.OpenDate);
    }

    [Fact]
    public async Task DeactivateAsync_SuccessPath_ShouldCallSaveChangesOnce()
    {
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());

        await _bankAccountService.DeactivateAsync(It.IsAny<string>(), It.IsAny<CancellationToken>());

        _unitOfWorkMock.Verify(unitOfWork => unitOfWork.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task CalculateCommissionAsync_SuccessPath_ShouldCallCheckForSameAccountsOnce()
    {
        const string firstAccountId = "TestFirstAccountId";
        const string secondAccountId = "TestSecondAccountId";

        await _bankAccountService.CalculateCommissionAsync(It.IsAny<double>(), firstAccountId, secondAccountId,
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(
            validatingMethods => validatingMethods.CheckForSameAccountsAndThrow(firstAccountId, secondAccountId),
            Times.Once);
    }

    [Fact]
    public async Task CalculateCommissionAsync_SuccessPath_ShouldCallEnsurePositiveOnce()
    {
        const int amount = 12345678;

        await _bankAccountService.CalculateCommissionAsync(amount, It.IsAny<string>(), It.IsAny<string>(),
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(validatingMethods => validatingMethods.EnsurePositive(amount), Times.Once);
    }

    [Fact]
    public async Task CalculateCommissionAsync_SuccessPath_ShouldGetUsersWithRightIds()
    {
        const string firstAccountId = "FirstTestAccountId";
        const string secondAccountId = "SecondTestAccountId";


        await _bankAccountService.CalculateCommissionAsync(It.IsAny<double>(), firstAccountId, secondAccountId,
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(
            validatingMethods => validatingMethods.GetActiveAccountAsync(firstAccountId, It.IsAny<CancellationToken>()),
            Times.Once);
        _accountValidatingMethodsMock.Verify(
            validatingMethods => validatingMethods.GetActiveAccountAsync(secondAccountId, It.IsAny<CancellationToken>()),
            Times.Once);
    }

    [Fact]
    public async Task CalculateCommissionAsync_SuccessPath_ShouldCallCalculateCommissionWithRightArgs()
    {
        const double amount = 12345678;
        const string firstAccountId = "FirstTestAccountId";
        const string secondAccountId = "SecondTestAccountId";
        var expectedFirstAccount = new BankAccount();
        var expectedSecondAccount = new BankAccount();
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(firstAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(expectedFirstAccount);
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(secondAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(expectedSecondAccount);

        await _bankAccountService.CalculateCommissionAsync(amount, firstAccountId, secondAccountId,
            It.IsAny<CancellationToken>());

        _commissionCounterMock.Verify(commissionCounter =>
            commissionCounter.CalculateCommission(amount, expectedFirstAccount, expectedSecondAccount));
    }

    //public async Task TransferMoneyAsync(double amount, string fromAccountId, string toAccountId, CancellationToken cancellationToken)
    //{
    //    _accountValidatingMethodsMock.CheckForSameAccountsAndThrow(fromAccountId, toAccountId);
    //    _accountValidatingMethodsMock.EnsurePositive(amount);

    //    var firstAccount = await _accountValidatingMethodsMock.GetActiveAccountAsync(fromAccountId, cancellationToken);
    //    var secondAccount = await _accountValidatingMethodsMock.GetActiveAccountAsync(toAccountId, cancellationToken);

    //    _accountValidatingMethodsMock.EnsureAccountHasEnoughMoney(amount, firstAccount);

    //    var amountWithCommission = await _commissionCounter.CalculateAmountWithCommission(amount, firstAccount, secondAccount);

    //    await _transactionRepository.AddTransactionAsync(new Transaction
    //    {
    //        Amount = amount,
    //        Currency = firstAccount.Currency,
    //        FromAccountId = fromAccountId,
    //        ToAccountId = toAccountId
    //    }, cancellationToken);

    //    firstAccount.Money -= amount;
    //    secondAccount.Money += amountWithCommission;

    //    await _bankAccountRepository.UpdateAsync(firstAccount, cancellationToken);
    //    await _bankAccountRepository.UpdateAsync(secondAccount, cancellationToken);
    //    await _unitOfWork.SaveChangesAsync(cancellationToken);
    //}

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallCheckForSameAccountsOnce()
    {
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());


        await _bankAccountService.TransferMoneyAsync(It.IsAny<double>(), fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(validatingMethods =>
            validatingMethods.CheckForSameAccountsAndThrow(fromAccountId, toAccountId), Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallEnsurePositiveOnce()
    {
        const int testAmount = 12345678;
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());


        await _bankAccountService.TransferMoneyAsync(testAmount, It.IsAny<string>(), It.IsAny<string>(),
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(validatingMethods =>
            validatingMethods.EnsurePositive(testAmount), Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallGetActiveAccountWithRightArgsOnce()
    {
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());

        await _bankAccountService.TransferMoneyAsync(It.IsAny<double>(), fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(validatingMethods =>
            validatingMethods.GetActiveAccountAsync(fromAccountId, It.IsAny<CancellationToken>()), Times.Once);
        _accountValidatingMethodsMock.Verify(validatingMethods =>
            validatingMethods.GetActiveAccountAsync(toAccountId, It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallEnsureAccountHasEnoughMoneyOnce()
    {
        const int testAmount = 12345678;
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        var expectedAccount = new BankAccount();
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(fromAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(expectedAccount);
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(toAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());

        await _bankAccountService.TransferMoneyAsync(testAmount, fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        _accountValidatingMethodsMock.Verify(validatingMethods =>
            validatingMethods.EnsureAccountHasEnoughMoney(testAmount, expectedAccount), Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallCountCommissionWithRightArgsOnce()
    {
        const int testAmount = 12345678;
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        var firstExpectedAccount = new BankAccount();
        var secondExpectedAccount = new BankAccount();
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(fromAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(firstExpectedAccount);
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(toAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(secondExpectedAccount);

        await _bankAccountService.TransferMoneyAsync(testAmount, fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        _commissionCounterMock.Verify(commissionCounter =>
                commissionCounter.CalculateAmountWithCommission(testAmount, firstExpectedAccount, secondExpectedAccount),
            Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallAddTransactionOnce()
    {
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());


        await _bankAccountService.TransferMoneyAsync(It.IsAny<double>(), It.IsAny<string>(), It.IsAny<string>(),
            It.IsAny<CancellationToken>());

        _transactionRepositoryMock.Verify(transaction =>
            transaction.AddTransactionAsync(It.IsAny<Transaction>(), It.IsAny<CancellationToken>()),
                Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldChangeFirstAccountMoneyRight()
    {
        const double amount = 123456;
        const double firstAccountMoney = 12345678;
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        var firstExpectedAccount = new BankAccount {Money = firstAccountMoney};
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(fromAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(firstExpectedAccount);
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(toAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());


        await _bankAccountService.TransferMoneyAsync(amount, fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        Assert.Equal(firstAccountMoney - amount, firstExpectedAccount.Money);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldChangeSecondAccountMoneyRight()
    {
        const double amount = 123456;
        const double secondAccountMoney = 12345678;
        const double amountWithCommission = 12345;
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        var secondExpectedAccount = new BankAccount { Money = secondAccountMoney };
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(fromAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(toAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(secondExpectedAccount);
        _commissionCounterMock.Setup(commissionCounter =>
            commissionCounter.CalculateAmountWithCommission(amount, It.IsAny<BankAccount>(), secondExpectedAccount))
            .ReturnsAsync(amountWithCommission);


        await _bankAccountService.TransferMoneyAsync(amount, fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        Assert.Equal(secondAccountMoney + amountWithCommission, secondExpectedAccount.Money);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallUpdateWithRightAccount()
    {
        const string fromAccountId = "TestFromAccountId";
        const string toAccountId = "TestToAccountId";
        var firstExpectedAccount = new BankAccount();
        var secondExpectedAccount = new BankAccount();
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(fromAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(firstExpectedAccount);
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(toAccountId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(secondExpectedAccount);

        await _bankAccountService.TransferMoneyAsync(It.IsAny<double>(), fromAccountId, toAccountId,
            It.IsAny<CancellationToken>());

        _bankAccountRepositoryMock.Verify(
            accountRepository => accountRepository.UpdateAsync(firstExpectedAccount, It.IsAny<CancellationToken>()),
            Times.Once);
        _bankAccountRepositoryMock.Verify(
            accountRepository => accountRepository.UpdateAsync(secondExpectedAccount, It.IsAny<CancellationToken>()),
            Times.Once);
    }

    [Fact]
    public async Task TransferMoneyAsync_SuccessPath_ShouldCallSaveChangesOnce()
    {
        _accountValidatingMethodsMock.Setup(validatingMethods =>
                validatingMethods.GetActiveAccountAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(new BankAccount());

        await _bankAccountService.TransferMoneyAsync(It.IsAny<double>(), It.IsAny<string>(), It.IsAny<string>(),
            It.IsAny<CancellationToken>());

        _unitOfWorkMock.Verify(unitOfWork => unitOfWork.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
    }
}