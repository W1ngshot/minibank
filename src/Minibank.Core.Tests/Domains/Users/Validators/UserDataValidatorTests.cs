﻿using System.Linq;
using System.Threading.Tasks;
using Minibank.Core.Domains.Users;
using Minibank.Core.Domains.Users.Validators;
using Minibank.Core.ValidationMessages;
using Xunit;

namespace Minibank.Core.Tests.Domains.Users.Validators;

public class UserDataValidatorTests
{
    private readonly UserDataValidator _validator;
    public UserDataValidatorTests()
    {
        _validator = new UserDataValidator();
    }   

    [Theory]
    [InlineData("", ErrorMessages.EmptyLogin)]
    [InlineData("    ", ErrorMessages.EmptyLogin)]
    [InlineData("a", ErrorMessages.TooShortLogin)]
    [InlineData("RandomTooLongUserLogin", ErrorMessages.TooLongLogin)]
    [InlineData("/", ErrorMessages.LoginContainsWrongSymbols)]
    public async Task UserDataValidator_LoginInput_ShouldHaveExpectedError(string login, string expectedResult)
    {
        var user = new User {Login = login};

        var result = await _validator.ValidateAsync(user);

        Assert.True(result.Errors?.Select(x => x.ErrorMessage).Contains(expectedResult));
    }

    [Theory]
    [InlineData("", ErrorMessages.EmptyEmail)]
    [InlineData("    ", ErrorMessages.EmptyEmail)]
    [InlineData("random", ErrorMessages.IncorrectEmail)]
    [InlineData("random@", ErrorMessages.IncorrectEmail)]
    [InlineData("random.", ErrorMessages.IncorrectEmail)]
    [InlineData("random@mail", ErrorMessages.IncorrectEmail)]
    [InlineData("random.mail", ErrorMessages.IncorrectEmail)]
    [InlineData("random@mail.", ErrorMessages.IncorrectEmail)]
    public async Task UserDataValidator_EmailInput_ShouldHaveExpectedError(string email, string expectedResult)
    {
        var user = new User { Email = email };

        var result = await _validator.ValidateAsync(user);

        Assert.True(result.Errors?.Select(x => x.ErrorMessage).Contains(expectedResult));
    }
}