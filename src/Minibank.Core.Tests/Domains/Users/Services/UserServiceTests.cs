using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Minibank.Core.Domains.Accounts;
using Minibank.Core.Domains.Users;
using Minibank.Core.Domains.Users.Services;
using Minibank.Core.ValidationMessages;
using Moq;
using Xunit;
using ValidationException = Minibank.Core.Exceptions.ValidationException;

namespace Minibank.Core.Tests.Domains.Users.Services;

public class UserServiceTests
{
    private readonly Mock<IUserRepository> _userRepositoryMock;
    private readonly Mock<IBankAccountRepository> _bankAccountRepositoryMock;
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IValidator<User>> _userDataValidatorMock;
    private readonly IUserService _userService;

    public UserServiceTests()
    {
        _userRepositoryMock = new Mock<IUserRepository>();
        _bankAccountRepositoryMock = new Mock<IBankAccountRepository>();
        _unitOfWorkMock = new Mock<IUnitOfWork>();
        _userDataValidatorMock = new Mock<IValidator<User>>();

        _userService = new UserService(_userRepositoryMock.Object,
            _bankAccountRepositoryMock.Object,
            _unitOfWorkMock.Object,
            _userDataValidatorMock.Object);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallCreateWithRightUser()
    {
        var testUser = new User {Login = "TestLogin", Email = "TestEmail"};

        await _userService.CreateAsync(testUser, It.IsAny<CancellationToken>());

        _userRepositoryMock.Verify(userRepository => userRepository
            .CreateAsync(testUser, It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallSaveChangesOnce()
    {
        await _userService.CreateAsync(new User(), It.IsAny<CancellationToken>());

        _unitOfWorkMock.Verify(unitOfWork => unitOfWork
            .SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task CreateAsync_SuccessPath_ShouldCallValidatorOnce()
    {
        await _userService.CreateAsync(new User(), It.IsAny<CancellationToken>());

        _userDataValidatorMock.Verify(validator => validator
            .ValidateAsync(It.IsAny<ValidationContext<User>>(), It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task CreateAsync_LoginOrEmailAlreadyRegistered_ShouldThrow()
    {
        var testUser = new User { Login = "TestLogin", Email = "TestEmail" };

        _userRepositoryMock.Setup(userRepository => userRepository
                .IsLoginOrEmailRegisteredAsync(testUser.Login, testUser.Email, It.IsAny<CancellationToken>(), null))
            .ReturnsAsync(true);

        var exception = await Assert.ThrowsAsync<ValidationException>(() =>
            _userService.CreateAsync(testUser, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.LoginOrEmailAlreadyExists, exception.Message);
    }

    [Fact]
    public async Task UpdateAsync_SuccessPath_ShouldCallUpdateWithRightUser()
    {
        var testUser = new User {Id = "TestId", Login = "TestLogin", Email = "TestEmail" };
        _userRepositoryMock.Setup(userRepository => 
                userRepository.IsExistsAsync(testUser.Id, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);


        await _userService.UpdateAsync(testUser, It.IsAny<CancellationToken>());

        _userRepositoryMock.Verify(userRepository => userRepository
            .UpdateAsync(testUser, It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task UpdateAsync_SuccessPath_ShouldCallSaveChangesOnce()
    {
        var testUser = new User { Id = "TestId", Login = "TestLogin", Email = "TestEmail" };
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(testUser.Id, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);

        await _userService.UpdateAsync(testUser, It.IsAny<CancellationToken>());

        _unitOfWorkMock.Verify(unitOfWork => unitOfWork
            .SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task UpdateAsync_SuccessPath_ShouldCallValidatorOnce()
    {
        var testUser = new User { Id = "TestId", Login = "TestLogin", Email = "TestEmail" };
        _userRepositoryMock
            .Setup(userRepository => userRepository
                .IsExistsAsync(testUser.Id, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);

        await _userService.UpdateAsync(testUser, It.IsAny<CancellationToken>());

        _userDataValidatorMock.Verify(validator => validator
            .ValidateAsync(It.IsAny<ValidationContext<User>>(), It.IsAny<CancellationToken>()), Times.Once);
    }


    [Fact]
    public async Task UpdateAsync_UserNotExist_ShouldThrowException()
    {
        var testUser = new User { Id = "TestId", Login = "TestLogin", Email = "TestEmail" };
        _userRepositoryMock
            .Setup(userRepository => userRepository
                .IsExistsAsync(testUser.Id, It.IsAny<CancellationToken>()))
            .ReturnsAsync(false);

        var exception = await Assert
            .ThrowsAsync<ValidationException>(() => _userService
                .UpdateAsync(testUser, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.UserNotExists, exception.Message);
    }

    [Fact]
    public async Task UpdateAsync_LoginOrEmailAlreadyRegistered_ShouldThrow()
    {
        var testUser = new User {Id = "TestId", Login = "TestLogin", Email = "TestEmail" };
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(testUser.Id, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);
        _userRepositoryMock.Setup(userRepository => userRepository
                .IsLoginOrEmailRegisteredAsync(testUser.Login, testUser.Email, It.IsAny<CancellationToken>(), testUser.Id))
            .ReturnsAsync(true);

        var exception = await Assert.ThrowsAsync<ValidationException>(() =>
            _userService.UpdateAsync(testUser, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.LoginOrEmailAlreadyExists, exception.Message);
    }

    [Fact]
    public async Task DeleteAsync_SuccessPath_ShouldCallDeleteWithRightId()
    {
        const string testId = "TestId";
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);
        _bankAccountRepositoryMock.Setup(bankAccountRepository =>
                bankAccountRepository.HasAccountsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(false);


        await _userService.DeleteAsync(testId, It.IsAny<CancellationToken>());

        _userRepositoryMock.Verify(userRepository => userRepository
            .DeleteAsync(testId, It.IsAny<CancellationToken>()), Times.Once);
    }

    [Fact]
    public async Task DeleteAsync_SuccessPath_ShouldCallSaveChangesOnce()
    {
        const string testId = "TestId";
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);
        _bankAccountRepositoryMock.Setup(bankAccountRepository =>
                bankAccountRepository.HasAccountsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(false);

        await _userService.DeleteAsync(testId, It.IsAny<CancellationToken>());

        _unitOfWorkMock.Verify(unitOfWork => unitOfWork
            .SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
    }


    [Fact]
    public async Task DeleteAsync_UserNotExist_ShouldThrowException()
    {
        const string testId = "TestId";
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(false);
        _bankAccountRepositoryMock.Setup(bankAccountRepository =>
                bankAccountRepository.HasAccountsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(false);

        var exception = await Assert
            .ThrowsAsync<ValidationException>(() => _userService
                .DeleteAsync(testId, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.UserNotExists, exception.Message);
    }

    [Fact]
    public async Task DeleteAsync_HasAccounts_ShouldThrowException()
    {
        const string testId = "TestId";
        _userRepositoryMock.Setup(userRepository =>
                userRepository.IsExistsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);
        _bankAccountRepositoryMock.Setup(bankAccountRepository =>
                bankAccountRepository.HasAccountsAsync(testId, It.IsAny<CancellationToken>()))
            .ReturnsAsync(true);

        var exception = await Assert
            .ThrowsAsync<ValidationException>(() => _userService
                .DeleteAsync(testId, It.IsAny<CancellationToken>()));

        Assert.Equal(ErrorMessages.UserHasAccounts, exception.Message);
    }
}